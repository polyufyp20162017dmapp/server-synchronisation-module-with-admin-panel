<?php

	$json = file_get_contents('php://input');
	$obj = json_decode($json);
	
	if ($obj!=null ){
		
		require_once('mysql.inc.php');
		require_once('forms.inc.php');
		
		//echo $json;
		$userId = $obj->{'userId'};
		$reminderDetail = $obj->{'reminderDetail'};
		
		// Title of CSV
		$cls[] =  array('Account', 'Date_update', 'Date of Birth', 'Age', 'Name', 'Gender','Smoker','Insulin_inject','Height_unit', 'Weight_unit', 'Waist_unit', 'BodyIndex_Height(cm)', 'BodyIndex_Weight(kg)', 'BodyIndex_BMI', 'BodyIndex_Waist_Circumference(cm)', 'BodyIndex_HbA1c', 'BodyIndex_LDL-C', 'BodyIndex_HDL-C','BodyIndex_Triglycerides', 'BodyIndex_total_cholesterol', 'BodyIndex_Remarks', 'Med_name', 'Med_times', 'Med_alarm1','Med_alarm2','Med_alarm3','Med_alarm4', 'Med_remarks');
		
		// get User Information
		$sql_infor="select USER_ID,CURDATE() AS CUR_DATE, BIRTHDAY, TIMESTAMPDIFF(YEAR,BIRTHDAY,CURDATE()) AS AGE, NAME, IF(SEX = 0, 'F', 'M') AS GENDER, SMOKER , IF(HAS_INJECTION = 'N', '0', '1') as INJECTION, IF(HEIGHT_UNIT = 0, 'cm', 'feet') AS Height_unit, IF(WEIGHT_UNIT = 0, 'kg', 'lb') AS Weight_unit, IF(WAIST_UNIT = 0, 'cm', 'inch') AS Waist_unit FROM DM_USER where USER_ID='$userId'";
		
		$result_infor = select_result($sql_infor);
		if (get_rowCount($result_infor)!=0){
			while($row_infor = get_row_mysqli_assoc($result_infor)){
				$userId = $row_infor['USER_ID'];
				$curDate = $row_infor['CUR_DATE'];
				$birthday = $row_infor['BIRTHDAY'];
				$age = $row_infor['AGE'];
				$name = $row_infor['NAME'];
				$gender = $row_infor['GENDER'];
				$smoker = $row_infor['SMOKER'];
				$injection = $row_infor['INJECTION'];
				$heightUnit = $row_infor['Height_unit'];
				$weightUnit = $row_infor['Weight_unit'];
				$waistUnit = $row_infor['Waist_unit'];
			}
			
			// export result of Blood tests
			$sql_detail = "select DATE,HEIGHT,WEIGHT,BMI,WAIST, ''as HbA1c , ''as LDL_C, ''as HDL_C, ''as TRIGLYCERIDES, ''as TOTAL_C, ''as REMARKS from BODY_RECORD where USER_ID='$userId' and RECORD_TYPE = 2 UNION ALL select DATE,'','','','',HbA1c,'','','','','' from BODY_RECORD where USER_ID='$userId' and RECORD_TYPE = 3 UNION ALL select DATE,'','','','','',LDL_C,HDL_C,TRIGLYCERIDES,TOTAL_C, REMARKS from BODY_RECORD where USER_ID='$userId' and RECORD_TYPE = 6 order by DATE";
			
			$result_detail = select_result($sql_detail);
			if (get_rowCount($result_detail)!=0){
				while($row_detail = get_row_mysqli_assoc($result_detail)){
					$cls[]=array($userId,$row_detail['DATE'], $birthday, $age, $name, $gender, $smoker, $injection, $heightUnit, $weightUnit, $waistUnit, $row_detail['HEIGHT'],$row_detail['WEIGHT'],$row_detail['BMI'],$row_detail['WAIST'],$row_detail['HbA1c'],$row_detail['LDL_C'],$row_detail['HDL_C'] ,$row_detail['TRIGLYCERIDES'] ,$row_detail['TOTAL_C'] ,$row_detail['REMARKS']  );
				}
			}
			
			// export result of reminder
			foreach ($reminderDetail as $reminderDetail) {
				//$alarm = $reminderDetail->time1 ." ".$reminderDetail->time2 . " " . $reminderDetail->time3 . " " . $reminderDetail->time4;
				$cls[]=array($userId, $curDate, $birthday, $age, $name, $gender, $smoker, $injection, $heightUnit, $weightUnit, $waistUnit,"","","","","","","","" ,"" ,"",$reminderDetail->drugName,$reminderDetail->manyTime, $reminderDetail->time1,$reminderDetail->time2,$reminderDetail->time3,$reminderDetail->time4, $reminderDetail->remark );
			}
		}
		
		header('Content-Type: application/csv; charset=UTF-8');
		header('Content-Disposition: attachment; filename="exportBodyIndexCSV.csv";');
		$f = fopen('php://output', "w");
		fprintf($f, chr(0xEF).chr(0xBB).chr(0xBF));
			foreach ($cls as $line) {
				fputcsv($f, $line);
			}
		fclose($f);
		// reset the file pointer to the start of the file
		//fseek($f, 0);
		//fpassthru($f);
		
		closeConnectDB();
	}
?>