<?php
	require_once('../mysql.inc.php');
	require_once('../forms.inc.php');
	
	// Check for a form submission:
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if ( isNotEmptyAndNotNull($_POST['user_id']) ){
			$userId = escape_data($_POST['user_id']);
	
			//$userId = escape_data('123456');
			//select a.FOOD_COMMON_COUNT, b.FOOD_ID, b.FOOD_NAME, b.PHOTO_PATH from FOOD_USER_COMMON a, FOOD b where a.USER_ID='123456' and a.FOOD_ID = b.FOOD_ID order by a.FOOD_COMMON_COUNT DESC LIMIT 20
			
			$sql="select b.FOOD_ID, b.FOOD_NAME, b.PHOTO_PATH from FOOD_USER_COMMON a, FOOD b where a.USER_ID='$userId' and a.FOOD_ID = b.FOOD_ID order by a.FOOD_COMMON_COUNT DESC LIMIT 20";
			
			$result = select_result($sql);
			if (get_rowCount($result)!=0){
				$i=0;
				while($row=get_row_mysqli_assoc($result)){
					$cls[$i]['FOOD_ID'] = $row['FOOD_ID'];
					$cls[$i]['FOOD_NAME'] = $row['FOOD_NAME'];
					if ($row['PHOTO_PATH']==null){
						$cls[$i]['PHOTO_IMAGE'] = "NA";
					}else{
						$cls[$i]['PHOTO_IMAGE'] = base64_encode(file_get_contents("../foodImage/" . $row['PHOTO_PATH'] . ".jpg" ));
					}
					$i++;
				}
				echo(json_encode($cls));
			}
		}
	}		
	
	closeConnectDB();
	
?>