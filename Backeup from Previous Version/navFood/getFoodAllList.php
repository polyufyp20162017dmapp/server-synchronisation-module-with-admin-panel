<?php
	require_once('../mysql.inc.php');
	require_once('../forms.inc.php');
	
	
	// Check for a form submission:
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if ( isNotEmptyAndNotNull($_POST['food_sub_cate_id']) ){
			$foddSubCateId = escape_data($_POST['food_sub_cate_id']);
			$sql="SELECT FOOD_ID, FOOD_NAME FROM FOOD WHERE FOOD_SUB_CATE_ID = " . $foddSubCateId;
			$result = select_result($sql);
			
			if (get_rowCount($result)!=0){
				while($row=get_row_mysqli_assoc($result)){
					$cls[]=$row;
				}
				echo(json_encode($cls));
			}
			
		}
	}		
	
	closeConnectDB();
	
?>