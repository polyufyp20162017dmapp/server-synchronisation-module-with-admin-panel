<?php
	require_once('../mysql.inc.php');
	require_once('../forms.inc.php');
	
	// Check for a form submission:
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if (isNotEmptyAndNotNull($_POST['user_id']) && isNotEmptyAndNotNull($_POST['food_record_id'])){
			$userId = escape_data($_POST['user_id']);
			$foodRecordId = escape_data($_POST['food_record_id']);

			//$userId = escape_data('123456');
			//$foodRecordId = escape_data('157');
	
			//SELECT FOOD_DATE, FOOD_TIME, FOOD_SESSION, FOOD_PLACE,FOOD_NAME,FOOD_UNIT,FOOD_QUANTITY FROM FOOD_RECORD WHERE USER_ID='123456' and FOOD_RECORD_ID='60'
	
			$sql="SELECT FOOD_DATE, FOOD_TIME, FOOD_SESSION, FOOD_PLACE,FOOD_NAME,FOOD_UNIT,FOOD_QUANTITY, PHOTO_PATH FROM FOOD_RECORD WHERE USER_ID='$userId' and FOOD_RECORD_ID='$foodRecordId'"  ;
			
			//echo $sql;
			
			$result = select_result($sql);
			
			if (get_rowCount($result)!=0){
				$i=0;
				while($row=get_row_mysqli_assoc($result)){
					$cls[$i]['FOOD_DATE'] = $row['FOOD_DATE'];
					$cls[$i]['FOOD_TIME'] = $row['FOOD_TIME'];
					$cls[$i]['FOOD_SESSION'] = $row['FOOD_SESSION'];
					$cls[$i]['FOOD_PLACE'] = $row['FOOD_PLACE'];
					$cls[$i]['FOOD_NAME'] = $row['FOOD_NAME'];
					$cls[$i]['FOOD_UNIT'] = $row['FOOD_UNIT'];
					$cls[$i]['FOOD_QUANTITY'] = $row['FOOD_QUANTITY'];
					$cls[$i]['PHOTO_PATH'] = $row['PHOTO_PATH'];
					if ($row['PHOTO_PATH']!=null){
						$cls[$i]['PHOTO_IMAGE'] = base64_encode(file_get_contents("userFoodImage/" . $row['PHOTO_PATH']));
					}
					$i++;
				}
				echo(json_encode($cls));
			}
		}
	}			
	
	closeConnectDB();
?>





