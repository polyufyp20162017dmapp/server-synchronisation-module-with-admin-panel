<?php
	require_once('../mysql.inc.php');
	require_once('../forms.inc.php');
	
	// Check for a form submission:
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if ( isNotEmptyAndNotNull($_POST['food_cate_id']) ){
			$foddCateId = escape_data($_POST['food_cate_id']);
			$sql="SELECT FOOD_SUB_CATE_ID,FOOD_SUB_CATE_NAME FROM FOOD_SUB_CATEGORIES WHERE FOOD_CATE_ID = " . $foddCateId;
			$result = select_result($sql);
			
			if (get_rowCount($result)!=0){
				while($row=get_row_mysqli_assoc($result)){
					$cls[]=$row;
				}
				echo(json_encode($cls));
			}
		}		
	}
	closeConnectDB();
	
?>