<?php
	require_once('../mysql.inc.php');
	require_once('../forms.inc.php');
	
	// Check for a form submission:
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if (isNotEmptyAndNotNull($_POST['user_id']) && isNotEmptyAndNotNull($_POST['recordType'])){
			$userId = escape_data($_POST['user_id']);
			$recordType = escape_data($_POST['recordType']);

			switch ($recordType) {
				
				case 1:{ //sql to get glucose record with ALL glucose record, record_type = 1
					$showType = escape_data($_POST['showType']);
					
						if ($showType == 0) { // get ALL records
						$sql="SELECT `ID`,`RECORD_TYPE`,`USER_ID`,`DATE`,`TIME`,`PERIOD`,`TYPE_GLUCOSE`,`GLUCOSE` FROM `BODY_RECORD` WHERE `USER_ID`='$userId' and `RECORD_TYPE`='$recordType' ORDER BY `DATE` DESC, `TIME` DESC LIMIT 300" ;
						break;}
						else { // get before or after meal records
						$sql="SELECT `ID`,`RECORD_TYPE`,`USER_ID`,`DATE`,`TIME`,`PERIOD`,`TYPE_GLUCOSE`,`GLUCOSE` FROM `BODY_RECORD` WHERE `USER_ID`='$userId' and `RECORD_TYPE`='$recordType' and `TYPE_GLUCOSE`='$showType' ORDER BY `DATE` DESC, `TIME` DESC LIMIT 300" ;
						break;}
											
				} 
				break;
			
				//sql to get height,weight,waist,bmi record, record_type = 2
				case 2:{
					$sql="SELECT `ID`, `RECORD_TYPE`, `USER_ID`, `DATE`, `TIME`, `HEIGHT`, `WEIGHT`, `WAIST`, `BMI` FROM `BODY_RECORD` WHERE `USER_ID`='$userId' and `RECORD_TYPE`='$recordType' ORDER BY `DATE` DESC, `TIME` DESC LIMIT 300 ";
				
				}
				break;
				//sql to get HbA1c record, record_type = 3
				case 3:{
					$sql="SELECT `ID`, `RECORD_TYPE`, `USER_ID`, `DATE`, `TIME`, `HbA1c`  FROM `BODY_RECORD` WHERE `USER_ID`='$userId' and `RECORD_TYPE`='$recordType' ORDER BY `DATE` DESC, `TIME` DESC LIMIT 300 ";					
				}				
				break;
				
				case 4:{ //sql to get BP,HEART_RATE record, record_type = 4
					
					$sql="SELECT `ID`,`RECORD_TYPE`,`USER_ID`,`DATE`,`TIME`,`BP_H`,`BP_L`,`HEART_RATE` FROM `BODY_RECORD`WHERE `USER_ID`='$userId' AND `RECORD_TYPE`='$recordType' ORDER BY `DATE` DESC, `TIME` DESC LIMIT 300" ;
				}	
				break;
					
				//sql to get LDL-C, HDL-C, TRIGLYCERIDERS record, record_type = 5
				case 5:{
					$sql="SELECT `ID`,`RECORD_TYPE`,`USER_ID`,`DATE`,`TIME`, `TOTAL_C`, `LDL_C`, `HDL_C`, `TRIGLYCERIDES` FROM `BODY_RECORD` WHERE `USER_ID`='$userId' AND `RECORD_TYPE`='$recordType' ORDER BY `DATE` DESC, `TIME` DESC LIMIT 300" ;
				}
				break;
				//sql to get remarks, record_type = 6
				case 6:{
					$sql="SELECT `ID`,`RECORD_TYPE`,`USER_ID`,`DATE`,`TIME`, `REMARKS` FROM `BODY_RECORD` WHERE `USER_ID`='$userId' AND `RECORD_TYPE`='$recordType' ORDER BY `DATE` DESC, `TIME` DESC LIMIT 300" ;
				}
				break;
				//sql to get mix type at report section, record_type = 7 (3,5,6 mix)
				case 7:{ 
					$sql="SELECT `ID`, `RECORD_TYPE`, `USER_ID`, `DATE`, `TIME`, `HbA1c`, `TOTAL_C`, `LDL_C`, `HDL_C`, `TRIGLYCERIDES`, `REMARKS` FROM `BODY_RECORD` WHERE `USER_ID`='$userId' AND (`RECORD_TYPE`=3 OR `RECORD_TYPE`=5 OR `RECORD_TYPE`=6) ORDER BY `DATE` DESC, `TIME` DESC LIMIT 300" ;
				}
				break;
			}
			
			//echo $sql;
			
			$result = select_result($sql);
			
			if (get_rowCount($result)!=0){
				while($row=get_row_mysqli_assoc($result)){
					$cls[]=$row;
				}
				echo(json_encode($cls));
			}
		}
	}			
	
	closeConnectDB();
?>





