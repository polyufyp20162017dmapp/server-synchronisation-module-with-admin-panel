<?php
	require_once('mysql.inc.php');
    
	$user_id = $_POST["user_id"];
    $name = $_POST["name"];
    $birthday = $_POST["birthday"];
    $sex = $_POST["sex"];
    $smoker = $_POST["smoker"];
	$injection = $_POST["injection"];
    $morning_start = $_POST["morning_start"];
	$morning_end = $_POST["morning_end"];
	$breakfast_start = $_POST["breakfast_start"];
	$breakfast_end = $_POST["breakfast_end"];
	$lunch_start = $_POST["lunch_start"];
	$lunch_end = $_POST["lunch_end"];
	$dinner_start = $_POST["dinner_start"];
	$dinner_end = $_POST["dinner_end"];
	$bed_start = $_POST["bed_start"];
	$bed_end = $_POST["bed_end"];
	$height_unit = $_POST["height_unit"];
	$weight_unit = $_POST["weight_unit"];
	$waist_unit = $_POST["waist_unit"];
	
	
    
    $statement = mysqli_prepare($dbc, "INSERT INTO `DM_USER` (`USER_ID`, `NAME`, `BIRTHDAY`, `SEX`, `SMOKER`, `HAS_INJECTION`,`MORNING_START_TIME`,
	`MORNING_END_TIME`, `BREAKFAST_START_TIME`,	`BREAKFAST_END_TIME`, `LUNCH_START_TIME`, `LUNCH_END_TIME`, `DINNER_START_TIME`, `DINNER_END_TIME`,
	`BED_START_TIME`, `BED_END_TIME`, `HEIGHT_UNIT`, `WEIGHT_UNIT`,	`WAIST_UNIT`) VALUES  (?, ?, ?, ? ,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    mysqli_stmt_bind_param($statement, "issiisssssssssssiii", $user_id, $name, $birthday, $sex, $smoker,$injection, $morning_start, $morning_end, $breakfast_start,
	$breakfast_end, $lunch_start, $lunch_end, $dinner_start, $dinner_end, $bed_start, $bed_end, $height_unit, $weight_unit, $waist_unit);
    mysqli_stmt_execute($statement);
    
    mysqli_stmt_close($statement);
    
	closeConnectDB();
?>
