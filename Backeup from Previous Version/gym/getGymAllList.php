<?php
	require_once('../mysql.inc.php');
	require_once('../forms.inc.php');
	
	
	// Check for a form submission:
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if ( isNotEmptyAndNotNull($_POST['gym_cate_id']) ){
			$gymSubCateId = escape_data($_POST['gym_cate_id']);
			$sql="SELECT EXERCISE_ID, EXERCISE_NAME FROM EXERCISE WHERE EXERCISE_CATE_ID = " . $gymSubCateId;
			$result = select_result($sql);
			
			if (get_rowCount($result)!=0){
				while($row=get_row_mysqli_assoc($result)){
					$cls[]=$row;
				}
				echo(json_encode($cls));
			}
			
		}
	}		
	
	closeConnectDB();
	
?>