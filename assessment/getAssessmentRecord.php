<?php
	require_once('../mysql.inc.php');
	require_once('../forms.inc.php');
	
	// Check for a form submission:
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if (isNotEmptyAndNotNull($_POST['user_id']) && isNotEmptyAndNotNull($_POST['assessment_week']) && isNotEmptyAndNotNull($_POST['assessment_year']) && isNotEmptyAndNotNull($_POST['start_week_date']) && isNotEmptyAndNotNull($_POST['end_week_date']) ){
			$userId = escape_data($_POST['user_id']);
			$week = escape_data($_POST['assessment_week']);
			$year = escape_data($_POST['assessment_year']);
			$startWeekDate = escape_data($_POST['start_week_date']);
			$endWeekDate = escape_data($_POST['end_week_date']);
	
			/*
			$userId = escape_data('555555');
			$year = escape_data('2016');
			$week = escape_data('14');
			$startWeekDate = escape_data('2016-3-27');
			$endWeekDate = escape_data('2016-4-2');
			*/
			
			//check smoker
			$i=0;
			$sql_smoker="SELECT SMOKER FROM DM_USER WHERE USER_ID = '$userId'"  ;
			$result_smoker = select_result($sql_smoker);
			if (get_rowCount($result_smoker)!=0){
				while($row_smoker =get_row_mysqli_assoc($result_smoker)){
					if ($row_smoker['SMOKER']== 0){
						$cls['SMOKER'] = "A";
					}else{
						$cls['SMOKER'] = "D";
					}
				}
			}
			
			//check BMI
			$sql_BMI = "SELECT BMI FROM BODY_RECORD WHERE RECORD_TYPE = 2 and USER_ID='$userId' ORDER BY DATE, TIME DESC LIMIT 1";
			$result_BMI = select_result($sql_BMI);
			if (get_rowCount($result_BMI)!=0){
				while($row_BMI =get_row_mysqli_assoc($result_BMI)){
					if ( 18.5 <= $row_BMI['BMI'] && $row_BMI['BMI'] <=22.9){
						$cls['BMI'] = "A";
					}else{
						$cls['BMI'] = "D";
					}
				}
			}else{
				$cls['BMI'] = "N";
			}
			
			//check HbA1c
			$sql_HbA1c = "SELECT HbA1c FROM BODY_RECORD WHERE RECORD_TYPE = 3 and USER_ID='$userId' ORDER BY DATE, TIME DESC LIMIT 1";
			$result_HbA1c = select_result($sql_HbA1c);
			if (get_rowCount($result_HbA1c)!=0){
				while($row_HbA1c =get_row_mysqli_assoc($result_HbA1c)){
					if ($row_HbA1c['HbA1c'] < 7){
						$cls['HbA1c'] = "A";
					}else{
						$cls['HbA1c'] = "D";
					}
				}
			}else{
				$cls['HbA1c'] = "N";
			}
			
			//check blood pressure
			$sql_bp = "SELECT BP_H, BP_L FROM BODY_RECORD WHERE RECORD_TYPE = 4 and USER_ID='$userId' and DATE between '$startWeekDate' and '$endWeekDate'";
			//echo $sql_bp;
			$result_bp = select_result($sql_bp);
			$count_bp_count = get_rowCount($result_bp);
			if ($count_bp_count!=0){
				$count_arrive_bp = 0;
				while($row_bp =get_row_mysqli_assoc($result_bp)){
					if ($row_bp['BP_H']<130 && $row_bp['BP_L']<80){
						$count_arrive_bp = $count_arrive_bp +1;
					}
				}
				$percentage_bp = $count_arrive_bp / $count_bp_count * 100;
				if ($percentage_bp >= 96){
					$cls['bp'] = "A";
				}else if ( 75 <= $percentage_bp && $percentage_bp <= 95){
					$cls['bp'] = "B";
				}else if ( 50 <= $percentage_bp && $percentage_bp <= 74){
					$cls['bp'] = "C";
				}else{
					$cls['bp'] = "D";
				}
			}else{
				$cls['bp'] = "N";
			}
			
			//Check cholesterol
			$sql_chol = "SELECT SEX,TOTAL_C,HDL_C,LDL_C,TRIGLYCERIDES FROM BODY_RECORD a, DM_USER b WHERE RECORD_TYPE = 5 and a.USER_ID='$userId' and a.USER_ID = b.USER_ID ORDER BY DATE, TIME DESC LIMIT 1";
			//echo $sql_chol;
			$result_chol = select_result($sql_chol);
			if (get_rowCount($result_chol)!=0){
				$count_arrive_chol = 0;
				while($row_chol =get_row_mysqli_assoc($result_chol)){
					if($row_chol['TOTAL_C']<4.5){
						$count_arrive_chol = $count_arrive_chol + 1;
					}
					if( ($row_chol['HDL_C']>1 && $row_chol['SEX']==1 ) || ($row_chol['HDL_C']>1.3 && $row_chol['SEX']==0 ) ){
						$count_arrive_chol = $count_arrive_chol + 1;
					}
					if($row_chol['LDL_C']<2.6){
						$count_arrive_chol = $count_arrive_chol + 1;
					}
					if($row_chol['TRIGLYCERIDES']<1.7){
						$count_arrive_chol = $count_arrive_chol + 1;
					}
					
					if($count_arrive_chol ==4){
						$cls['chol'] = "A";
					}else if($count_arrive_chol ==3){
						$cls['chol'] = "B";
					}else if($count_arrive_chol ==2){
						$cls['chol'] = "C";
					}else{
						$cls['chol'] = "D";
					}
				}
			}else{
				$cls['chol'] = "N";
			}
			
			//Check Food
			$sql_food="SELECT QUESTION_ANS1, QUESTION_ANS2, QUESTION_ANS3, QUESTION_ANS4, QUESTION_ANS5, QUESTION_ANS6 FROM FOOD_ASSESSMENT WHERE FOOD_ASSESSMENT_YEAR = '$year' and FOOD_ASSESSMENT_WEEK = '$week'  and USER_ID = '$userId'"  ;
			//echo $sql_food;
			$result_food = select_result($sql_food);
			if (get_rowCount($result_food)!=0){
				$countAnsA = 0;
				$countAnsB = 0;
				$countAnsC = 0;
				$countAnsD = 0;
				$countAnsN = 0;
				
				while($row_food =get_row_mysqli_assoc($result_food)){
					//QUESTION_ANS1
					if ($row_food['QUESTION_ANS1']=== "A"){
						$countAnsA = $countAnsA + 1;
					}
					if ($row_food['QUESTION_ANS1']=== "B"){
						$countAnsB = $countAnsB + 1;
					}
					if ($row_food['QUESTION_ANS1']=== "C"){
						$countAnsC = $countAnsC + 1;
					}
					if ($row_food['QUESTION_ANS1']=== "D"){
						$countAnsD = $countAnsD + 1;
					}
					if ($row_food['QUESTION_ANS1']=== "N"){
						$countAnsN = $countAnsN + 1;
					}
					//QUESTION_ANS2
					if ($row_food['QUESTION_ANS2']=== "A"){
						$countAnsA = $countAnsA + 1;
					}
					if ($row_food['QUESTION_ANS2']=== "B"){
						$countAnsB = $countAnsB + 1;
					}
					if ($row_food['QUESTION_ANS2']=== "C"){
						$countAnsC = $countAnsC + 1;
					}
					if ($row_food['QUESTION_ANS2']=== "D"){
						$countAnsD = $countAnsD + 1;
					}
					//QUESTION_ANS3
					if ($row_food['QUESTION_ANS3']=== "A"){
						$countAnsA = $countAnsA + 1;
					}
					if ($row_food['QUESTION_ANS3']=== "B"){
						$countAnsB = $countAnsB + 1;
					}
					if ($row_food['QUESTION_ANS3']=== "C"){
						$countAnsC = $countAnsC + 1;
					}
					if ($row_food['QUESTION_ANS3']=== "D"){
						$countAnsD = $countAnsD + 1;
					}
					//QUESTION_ANS4
					if ($row_food['QUESTION_ANS4']=== "A"){
						$countAnsA = $countAnsA + 1;
					}
					if ($row_food['QUESTION_ANS4']=== "B"){
						$countAnsB = $countAnsB + 1;
					}
					if ($row_food['QUESTION_ANS4']=== "C"){
						$countAnsC = $countAnsC + 1;
					}
					if ($row_food['QUESTION_ANS4']=== "D"){
						$countAnsD = $countAnsD + 1;
					}
					//QUESTION_ANS5
					if ($row_food['QUESTION_ANS5']=== "A"){
						$countAnsA = $countAnsA + 1;
					}
					if ($row_food['QUESTION_ANS5']=== "B"){
						$countAnsB = $countAnsB + 1;
					}
					if ($row_food['QUESTION_ANS5']=== "C"){
						$countAnsC = $countAnsC + 1;
					}
					if ($row_food['QUESTION_ANS5']=== "D"){
						$countAnsD = $countAnsD + 1;
					}		
					//QUESTION_ANS6
					if ($row_food['QUESTION_ANS6']=== "A"){
						$countAnsA = $countAnsA + 1;
					}
					if ($row_food['QUESTION_ANS6']=== "B"){
						$countAnsB = $countAnsB + 1;
					}
					if ($row_food['QUESTION_ANS6']=== "C"){
						$countAnsC = $countAnsC + 1;
					}
					if ($row_food['QUESTION_ANS6']=== "D"){
						$countAnsD = $countAnsD + 1;
					}					
				}
				//echo $countAnsA;
				//echo $countAnsB;
				//echo $countAnsC;
				//echo $countAnsD;
				//echo $countAnsN;
				if($countAnsA>=5 && $countAnsD==0){
					$cls['food'] = "A";
				}else if( ($countAnsA==0 && $countAnsC ==0 && $countAnsD ==0)  || ($countAnsA>3 && $countAnsC==0 && $countAnsD ==0) || ($countAnsD==1)   ){
					$cls['food'] = "B";
				}else if ( ($countAnsA==0 && $countAnsB ==0 && $countAnsD ==0) || (2<=$countAnsD && $countAnsD<=3)   ){
					$cls['food'] = "C";
				}else{
					$cls['food'] = "D";
				}
				
			}else{
				$cls['food'] = "N";
			}
			
			//Check exercise
			$sql_exe = "SELECT EXERCISE_TYPE_ID, EXERCISE_PERIOD FROM EXERCISE_TYPE_RECORD where USER_ID='$userId' and EXERCISE_DATE between '$startWeekDate' and '$endWeekDate'";
			//echo $sql_exe;
			$result_exe = select_result($sql_exe);
			if (get_rowCount($result_exe)!=0){
				$count_exe=0;
				$period = true;
				while($row_exe =get_row_mysqli_assoc($result_exe)){
					if($row_exe['EXERCISE_TYPE_ID'] ==2 ){
						$count_exe = $count_exe + 1;
						if ($row_exe['EXERCISE_PERIOD'] <30){
							$period = false;
						}
					}
				}
				if ($count_exe >= 3 && $period ){
					$cls['exe'] = "A";
				}else if ($count_exe >= 2){
					$cls['exe'] = "B";
				}else if ($count_exe == 1){
					$cls['exe'] = "C";
				}else{
					$cls['exe'] = "D";
				}
			}else{
				$cls['exe'] = "N";
			}
			
			echo(json_encode($cls));
		}
	}			
	
	closeConnectDB();
?>