/*
 Navicat Premium Data Transfer

 Source Server         : 158.132.8.53
 Source Server Type    : MySQL
 Source Server Version : 50552
 Source Host           : 158.132.8.53
 Source Database       : dma

 Target Server Type    : MySQL
 Target Server Version : 50552
 File Encoding         : utf-8

 Date: 05/05/2017 01:40:14 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `ADMIN_PANEL_USERS`
-- ----------------------------
DROP TABLE IF EXISTS `ADMIN_PANEL_USERS`;
CREATE TABLE `ADMIN_PANEL_USERS` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` text NOT NULL,
  `PASSWORD` text NOT NULL,
  `SALT` text NOT NULL,
  `ROLE` int(11) NOT NULL,
  `FAILED_LOGIN_COUNT` int(11) NOT NULL DEFAULT '0',
  `JAIL_START_TIME` datetime DEFAULT NULL,
  `LASTACTIVITY_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `BODY_RECORD`
-- ----------------------------
DROP TABLE IF EXISTS `BODY_RECORD`;
CREATE TABLE `BODY_RECORD` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECORD_TYPE` tinyint(1) NOT NULL,
  `USER_ID` int(6) NOT NULL,
  `DATE` date NOT NULL,
  `TIME` time NOT NULL,
  `HEIGHT` double DEFAULT NULL,
  `WEIGHT` double DEFAULT NULL,
  `WAIST` double DEFAULT NULL,
  `BMI` double DEFAULT NULL,
  `BP_H` int(3) DEFAULT NULL,
  `BP_L` int(3) DEFAULT NULL,
  `HEART_RATE` int(3) DEFAULT NULL,
  `HbA1c` double DEFAULT NULL,
  `PERIOD` int(1) DEFAULT NULL,
  `TYPE_GLUCOSE` tinyint(1) DEFAULT NULL,
  `GLUCOSE` double DEFAULT NULL,
  `FASTING_BLOOD_SUGAR` double DEFAULT NULL,
  `POST_GLUCOSE` double DEFAULT NULL,
  `TOTAL_C` double DEFAULT NULL,
  `LDL_C` double DEFAULT NULL,
  `HDL_C` double DEFAULT NULL,
  `TRIGLYCERIDES` double DEFAULT NULL,
  `REMARKS` mediumtext COLLATE utf8_unicode_ci,
  `create_datetime` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `edit_datetime` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`,`USER_ID`,`create_datetime`),
  UNIQUE KEY `create_datetime` (`create_datetime`)
) ENGINE=InnoDB AUTO_INCREMENT=3095 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `DM_USER`
-- ----------------------------
DROP TABLE IF EXISTS `DM_USER`;
CREATE TABLE `DM_USER` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(6) NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BIRTHDAY` date NOT NULL,
  `SEX` tinyint(1) NOT NULL DEFAULT '0',
  `SMOKER` tinyint(1) NOT NULL DEFAULT '0',
  `HAS_INJECTION` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `MORNING_START_TIME` time NOT NULL,
  `MORNING_END_TIME` time NOT NULL,
  `BREAKFAST_START_TIME` time NOT NULL,
  `BREAKFAST_END_TIME` time NOT NULL,
  `LUNCH_START_TIME` time NOT NULL,
  `LUNCH_END_TIME` time NOT NULL,
  `DINNER_START_TIME` time NOT NULL,
  `DINNER_END_TIME` time NOT NULL,
  `BED_START_TIME` time NOT NULL,
  `BED_END_TIME` time NOT NULL,
  `HEIGHT_UNIT` tinyint(1) NOT NULL DEFAULT '0',
  `WEIGHT_UNIT` tinyint(1) NOT NULL DEFAULT '0',
  `WAIST_UNIT` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `USER_ID` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `EXERCISE_TYPE_RECORD`
-- ----------------------------
DROP TABLE IF EXISTS `EXERCISE_TYPE_RECORD`;
CREATE TABLE `EXERCISE_TYPE_RECORD` (
  `EXERCISE_RECORD_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `USER_ID` int(6) NOT NULL DEFAULT '0',
  `EXERCISE_DATE` date DEFAULT NULL,
  `EXERCISE_TIME` time DEFAULT NULL,
  `EXERCISE_TYPE_ID` int(1) DEFAULT NULL,
  `EXERCISE_PERIOD` int(6) DEFAULT NULL,
  `create_datetime` varchar(30) NOT NULL,
  `edit_datetime` text NOT NULL,
  PRIMARY KEY (`EXERCISE_RECORD_ID`,`USER_ID`,`create_datetime`),
  UNIQUE KEY `create_datetime` (`create_datetime`)
) ENGINE=InnoDB AUTO_INCREMENT=235 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `FOOD_ASSESSMENT`
-- ----------------------------
DROP TABLE IF EXISTS `FOOD_ASSESSMENT`;
CREATE TABLE `FOOD_ASSESSMENT` (
  `FOOD_ASSESSMENT_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `USER_ID` int(6) DEFAULT NULL,
  `FOOD_ASSESSMENT_YEAR` varchar(4) DEFAULT NULL,
  `FOOD_ASSESSMENT_WEEK` varchar(2) DEFAULT NULL,
  `QUESTION_ANS1` char(1) DEFAULT NULL,
  `QUESTION_ANS2` char(1) DEFAULT NULL,
  `QUESTION_ANS3` char(1) DEFAULT NULL,
  `QUESTION_ANS4` char(1) DEFAULT NULL,
  `QUESTION_ANS5` char(1) DEFAULT NULL,
  `QUESTION_ANS6` char(1) DEFAULT NULL,
  PRIMARY KEY (`FOOD_ASSESSMENT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `FOOD_RECORD`
-- ----------------------------
DROP TABLE IF EXISTS `FOOD_RECORD`;
CREATE TABLE `FOOD_RECORD` (
  `FOOD_RECORD_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `USER_ID` int(6) NOT NULL DEFAULT '0',
  `FOOD_DATE` date DEFAULT NULL,
  `FOOD_TIME` time DEFAULT NULL,
  `FOOD_SESSION` char(1) DEFAULT NULL,
  `FOOD_PLACE` char(1) DEFAULT NULL,
  `FOOD_ID` int(6) DEFAULT NULL,
  `FOOD_QUANTITY` int(2) DEFAULT NULL,
  `FOOD_CALORIE` double DEFAULT NULL,
  `FOOD_CARBOHYDRATE` double DEFAULT NULL,
  `FOOD_PROTEIN` double DEFAULT NULL,
  `FOOD_FAT` double DEFAULT NULL,
  `FOOD_NAME` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `FOOD_UNIT` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `PHOTO_PATH` text,
  `create_datetime` varchar(30) NOT NULL,
  `edit_datetime` text NOT NULL,
  PRIMARY KEY (`FOOD_RECORD_ID`,`USER_ID`,`create_datetime`)
) ENGINE=InnoDB AUTO_INCREMENT=1664 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `FOOD_USER_COMMON`
-- ----------------------------
DROP TABLE IF EXISTS `FOOD_USER_COMMON`;
CREATE TABLE `FOOD_USER_COMMON` (
  `USER_ID` int(6) NOT NULL,
  `FOOD_ID` int(6) NOT NULL,
  `FOOD_COMMON_COUNT` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `INJECTION_RECORD`
-- ----------------------------
DROP TABLE IF EXISTS `INJECTION_RECORD`;
CREATE TABLE `INJECTION_RECORD` (
  `INJECTION_RECORD_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `USER_ID` int(6) NOT NULL,
  `INJECTION_DATE` date DEFAULT NULL,
  `INJECTION_TIME` time DEFAULT NULL,
  `INJECTION_NAME` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `INJECTION_VALUE` int(2) DEFAULT NULL,
  `create_datetime` varchar(30) NOT NULL,
  `edit_datetime` text,
  PRIMARY KEY (`INJECTION_RECORD_ID`,`create_datetime`,`USER_ID`),
  UNIQUE KEY `create_datetime` (`create_datetime`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `MEDICATION_RECORD`
-- ----------------------------
DROP TABLE IF EXISTS `MEDICATION_RECORD`;
CREATE TABLE `MEDICATION_RECORD` (
  `USER_ID` int(6) NOT NULL DEFAULT '0',
  `MEDICATION_DATE` date DEFAULT NULL,
  `HAS_TAKE_MEDICATION` char(1) DEFAULT NULL,
  `create_datetime` varchar(30) NOT NULL DEFAULT '',
  `edit_datetime` text,
  PRIMARY KEY (`USER_ID`,`create_datetime`),
  UNIQUE KEY `create_datetime` (`create_datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `REPORT_ACCESS_RECORDS`
-- ----------------------------
DROP TABLE IF EXISTS `REPORT_ACCESS_RECORDS`;
CREATE TABLE `REPORT_ACCESS_RECORDS` (
  `USER_ID` int(11) NOT NULL,
  `LAST_ACCESS_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `REPORT_SHARE_TOKEN`
-- ----------------------------
DROP TABLE IF EXISTS `REPORT_SHARE_TOKEN`;
CREATE TABLE `REPORT_SHARE_TOKEN` (
  `USER_ID` int(11) NOT NULL,
  `TOKEN` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `STEP_RECORD`
-- ----------------------------
DROP TABLE IF EXISTS `STEP_RECORD`;
CREATE TABLE `STEP_RECORD` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(6) NOT NULL,
  `DATE` date NOT NULL,
  `STEP_COUNT` int(11) NOT NULL,
  `create_datetime` varchar(30) NOT NULL,
  `edit_datetime` text NOT NULL,
  PRIMARY KEY (`ID`,`USER_ID`,`create_datetime`),
  KEY `ID` (`ID`,`USER_ID`,`create_datetime`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
