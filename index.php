<?php
require_once('./mysql.inc.php');

if ($_SERVER['REQUEST_METHOD'] != 'POST' || !isset($_POST['userId']) || !isset($_POST['event']) || !isset($_POST['method'])) {
    header('HTTP/1.0 404 Not Found');
  //print var_dump($_POST);
  exit();
}

$request_user_id = trim($_POST['userId']);
$request_event = trim($_POST['event']);
$request_method = trim($_POST['method']);
$request_data = (isset($_POST['data']))?json_decode(trim(urldecode($_POST['data']))):"";


//User Id Verify
$sql = 'SELECT ID FROM DM_USER WHERE USER_ID = '.$request_user_id;
$result = select_result($sql);

if (get_rowCount($result) <= 0) {
    header('HTTP/1.0 403 Forbidden');
    closeConnectDB();
    exit();
}

//Router
switch ($request_event) {
  case 'bodyrecord':
    require_once('./controller/bodyRecord.php');
    $bodyRecord = new bodyRecordContoller($request_user_id, $request_method, $request_data);
    break;

    case 'exercise':
      require_once('./controller/exercise.php');
      $bodyRecord = new exerciseController($request_user_id, $request_method, $request_data);
      break;

      case 'food':
        require_once('./controller/food.php');
        $bodyRecord = new foodRecordController($request_user_id, $request_method, $request_data);
        break;

        case 'step':
          require_once('./controller/stepRecord.php');
          $bodyRecord = new stepRecordContoller($request_user_id, $request_method, $request_data);
          break;

          case 'drug':
            require_once('./controller/drug.php');
            $bodyRecord = new drugContoller($request_user_id, $request_method, $request_data);
            break;

            case 'injection':
              require_once('./controller/injection.php');
              $bodyRecord = new injectionContoller($request_user_id, $request_method, $request_data);
              break;

              case 'report':
                require_once('./controller/report.php');
                $bodyRecord = new reportController($request_user_id, $request_method, $request_data);
                break;

  case 'syncdataversion':
    require_once('./controller/syncDataVersion.php');
    $syncDataVersion = new syncDataVersionController($request_user_id, $request_method, $request_data);
    break;

  case 'uploadImage':
  require_once('./controller/uploadImage.php');
  $filename = trim($_POST['filename']);
  $image = trim($_POST['image']);
  $syncDataVersion = new uploadImageController($filename, $image);
  break;

  default:
    header('HTTP/1.0 404 Not Found');
    closeConnectDB();
    exit();
    break;
}
closeConnectDB();
unset($request_data);
unset($request_method);
unset($result);
unset($request_event);
unset($request_user_id);
