<?php
session_start();

require_once(dirname(__FILE__, 4).'/mysql.inc.php');
require_once(dirname(__FILE__, 4).'/mysql.inc.php');
require_once(dirname(__FILE__, 4).'/libs/otphp/src/TOTP.php');
require_once(dirname(__FILE__, 4).'/libs/otphp/src/OTP.php');
require_once(dirname(__FILE__, 4).'/libs/otphp/src/Base32.php');
require_once dirname(__FILE__, 4).'/libs/dompdf/autoload.inc.php';

use OTPHP\TOTP;
use Base32\Base32;
use Dompdf\Dompdf;

class reportContoller
{

    public function __construct()
    {

    }

    public function tokenCheck(&$token)
    {
      $output = array();
      if (strlen(strval($token)) != 12 && is_numeric($token)) {
        $output['success'] = "false";
        $output['msg'] = "Input token is not correct!";
      }

      $userid = substr(strval($token), 0, 6);
      $key = substr(strval($token), 6, 6);

      $sql = "select * from `REPORT_SHARE_TOKEN` where `REPORT_SHARE_TOKEN`.`USER_ID` = ".intval($userid)." AND `REPORT_SHARE_TOKEN`.`TOKEN` = '".$key."';";
      $result = select_result($sql);
      if (get_rowCount($result) <= 0) {
        $output['success'] = "false";
        $output['msg'] = "Input token is not correct!";
      }else{
        $base32_userid = Base32::encode($userid);
        $totp = new TOTP(
            $userid, // The label (string)
            $base32_userid, // The secret encoded in base 32 (string)
            60*5,                 // The period (int)
            'sha512',           // The digest algorithm (string)
            6                   // The number of digits (int)
        );

        if (!$totp->verify($key)) {
          $output['success'] = "false";
          $output['msg'] = "Input token is timed out!";
        }else{
          $_SESSION['report_userid'] = $userid;
          $output['success'] = "true";
          $output['msg'] = "";
        }
      }
      print json_encode($output);
    }

    public function fetchReport(&$from_year, &$to_year, &$from_month, &$to_month) {
      $request_user_id = $_SESSION['report_userid'];

      function periodTolabel($label)
      {
        switch ($label) {
          case '1':
            return "早餐";
            break;

            case '2':
            return "午餐";
              break;

              case '3':
              return "晚餐";
                break;

          default:
          return "-";
            break;
        }
      }

      function glucoseTypeTolabel($label)
      {
        switch ($label) {
          case '1':
            return "餐前";
            break;

            case '2':
            return "餐後";
              break;

          default:
          return "-";
            break;
        }
      }

      function foodSessionToLabel($label)
      {
        switch ($label) {
          case 'S':
            return "小食";
            break;

            case 'L':
            return "午餐";
              break;

              case 'B':
              return "早餐";
                break;

                case 'D':
                return "晚餐";
                  break;

          default:
          return "-";
            break;
        }
      }

      function foodPlaceToLabel($label)
      {
        switch ($label) {
          case 'H':
            return "在家準備";
            break;

            case 'T':
            return "出外用膳";
              break;

              case 'O':
              return "外賣";
                break;

          default:
          return "-";
            break;
        }
      }


      //Prepare data
      $sql = 'SELECT * FROM DM_USER WHERE USER_ID = '.$request_user_id;
      $result = select_result($sql);
      $row1=get_row_mysqli_assoc($result);
      $userinfo = $row1;

      // instantiate and use the dompdf class
      $dompdf = new Dompdf();
      $dompdf->set_option("isPhpEnabled", true);

      //$dompdf->loadHtml('<h1>Welcome to CodexWorld.com</h1>');
      //$html = file_get_contents(dirname(__FILE__, 1).'/file.html');
      $html = "<html>
      <head>
        <style media='screen'>

        @font-face {
            font-family: 'dfming';
            font-style: normal;
            font-weight: 400;
            src: url(./fonts/DFMingStd-W7-BIG.ttf) format('truetype');
          }

          @font-face {
              font-family: 'dfming';
              font-style: normal;
              font-weight: 700;
              src: url(./fonts/DFMingStd-W14-BIG.ttf) format('truetype');
            }

          * {
            font-family: dfming, sans-serif;
          }

          page-break-before {
            page-break-before: always;
          }

          page-break-after {
            page-break-after: always;
          }

        </style>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
      </head>
      <body>
      <table border=0 style='margin-left:90px'>
      <tr>
      <td style='padding-right:20px;' rowspan=3><img src='../../report/imgs/image001.png' width='100px' hight='200px'/></td>
      <td style='padding-right:20px;' rowspan=3><img src='../../report/imgs/image003.png' width='100px' hight='170px'/></td>
      <td style='font-size:20px; text-align: center; font-weight: 900; padding-right:20px;'>廣華醫院家庭醫學及全科門診部</td>
      <td rowspan=3><img src='../../report/imgs/image005.png' width='100px' hight='200px'/></td>
      </tr>
      <tr>
      <td style='font-size:20px; text-align: center; font-weight: 900;'>香港理工大學電子及計算學系</td>
      </tr>
      <tr>
      <td style='font-size:20px; text-align: center; font-weight: 900;'>聯合製作</td>
      </tr>
      </table>

      <br />
      <table border=0 align='center'>
      <tr>
      <td style='font-size:20px; text-align: center; font-weight: 900;'>智能控糖應用程式</td>
      </tr>
      <tr>
      <td style='font-size:20px; text-align: center; font-weight: 900;'>記錄報告</td>
      </tr>
      </table>

      <br />
      <table border=0 align='center'>
      <tr>
      <td style='font-size:20px; text-align: center; font-weight: 900;'>".(date("Y-m-d H:i"))."</td>
      </tr>
      </table>

      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />

      <table border=0 align='center' style='border-top:1pt solid black; border-bottom:1pt solid black;'>
      <tr>
      <td style='text-align: center; font-weight: 900; width: 300px;'>帳戶號碼:</td>
      <td style='text-align: center; width: 300px;'>".$userinfo['USER_ID']."</td>
      </tr>
      <tr>
      <td style='text-align: center; font-weight: 900; width: 300px;'>用戶名稱:</td>
      <td style='text-align: center; width: 300px;'>".$userinfo['NAME']."</td>
      </tr>
      <tr>
      <td style='text-align: center; font-weight: 900; width: 300px;'>出生日期:</td>
      <td style='text-align: center; width: 300px;'>".$userinfo['BIRTHDAY']."</td>
      </tr>
      <tr>
      <td style='text-align: center; font-weight: 900; width: 300px;'>性別:</td>
      <td style='text-align: center; width: 300px;'>".(($userinfo['SEX'] == 0)?"女":"男")."</td>
      </tr>
      <tr>
      <td style='text-align: center; font-weight: 900; width: 300px;'>吸煙者:</td>
      <td style='text-align: center; width: 300px;'>".(($userinfo['SMOKER'] == 0)?"不是":"是")."</td>
      </tr>
      <tr>
      <td style='text-align: center; font-weight: 900; width: 300px;'>需要打針:</td>
      <td style='text-align: center; width: 300px;'>".(($userinfo['HAS_INJECTION'] == "N")?"不需要":"需要")."</td>
      </tr>
      </table>
      ";
      $html = $html.'<script type="text/php">
              if ( isset($pdf) ) {
                  $x = 400;
                  $y = 560;
                  $text = "{PAGE_NUM} of {PAGE_COUNT}";
                  $font = $fontMetrics->get_font("dfming", "bold");
                  $size = 12;
                  $word_space = 0.0;  //  default
                  $char_space = 0.0;  //  default
                  $angle = 0.0;   //  default
                  $color = array(0,0,0);
                  $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
              }
          </script>';

      $sql = 'select YEAR(`BODY_RECORD`.`DATE`) as year, MONTH(`BODY_RECORD`.`DATE`) as month from `BODY_RECORD` where `BODY_RECORD`.`RECORD_TYPE` = 1 AND `BODY_RECORD`.`USER_ID` = '.$request_user_id.' and (DATE BETWEEN "'.$from_year.'-'.$from_month.'-01 00:00:00" AND "'.$to_year.'-'.$to_month.'-31 00:00:00") GROUP BY YEAR(`BODY_RECORD`.`DATE`), MONTH(`BODY_RECORD`.`DATE`) ASC';
      $result = select_result($sql);
      if (get_rowCount($result)!=0) {
        while ($row1=get_row_mysqli_assoc($result)) {
          $html = $html."<page-break-after></page-break-after>
          <p style='font-size:20px; font-weight: 900;'>血糖記錄 ".$row1['year']." / ".$row1['month']."</p><br />

          <table style='width: 1000px;'>
          <thead style='border-bottom:1pt solid black;'>
          <tr style='text-align: center; font-weight: 900; width: 300px;'>
          <th>日期</th>
          <th>時間</th>
          <th>時段</th>
          <th>類型</th>
          <th>血糖值</th>
          </tr>
          </thead>
          <tbody>";

          $sql = "select * from `BODY_RECORD` where `BODY_RECORD`.`RECORD_TYPE` = 1 AND `BODY_RECORD`.`USER_ID` = ".$request_user_id." AND YEAR(`BODY_RECORD`.`DATE`) = ". $row1['year'] ." AND MONTH(`BODY_RECORD`.`DATE`) = ". $row1['month'] ." ORDER BY `BODY_RECORD`.`DATE`, `BODY_RECORD`.`TIME` ASC";
          $result2 = select_result($sql);
          if (get_rowCount($result2)!=0) {
            while ($row2=get_row_mysqli_assoc($result2)) {
              $html = $html."<tr style='text-align: center; font-weight: 900; width: 300px;'>
              <td>".$row2['DATE']."</td>
              <td>".$row2['TIME']."</td>
              <td>".periodTolabel($row2['PERIOD'])."</td>
              <td>".glucoseTypeTolabel($row2['TYPE_GLUCOSE'])."</td>
              <td>".$row2['GLUCOSE']."</td>
              </tr>";
            }
          }

          $html = $html."</tbody>
          </table>";
        }
      }


      /////// BP

      $sql = 'select YEAR(`BODY_RECORD`.`DATE`) as year, MONTH(`BODY_RECORD`.`DATE`) as month from `BODY_RECORD` where `BODY_RECORD`.`RECORD_TYPE` = 4 AND `BODY_RECORD`.`USER_ID` = '.$request_user_id.' and (DATE BETWEEN "'.$from_year.'-'.$from_month.'-01 00:00:00" AND "'.$to_year.'-'.$to_month.'-31 00:00:00") GROUP BY YEAR(`BODY_RECORD`.`DATE`), MONTH(`BODY_RECORD`.`DATE`) ASC';
      $result = select_result($sql);
      if (get_rowCount($result)!=0) {
        while ($row1=get_row_mysqli_assoc($result)) {
          $html = $html."<page-break-after></page-break-after>
          <p style='font-size:20px; font-weight: 900;'>血壓記錄 ".$row1['year']." / ".$row1['month']."</p><br />

          <table style='width: 1000px;'>
          <thead style='border-bottom:1pt solid black;'>
          <tr style='text-align: center; font-weight: 900; width: 300px;'>
          <th>日期</th>
          <th>時間</th>
          <th>上壓</th>
          <th>下壓</th>
          <th>心跳</th>
          </tr>
          </thead>
          <tbody>";

          $sql = "select * from `BODY_RECORD` where `BODY_RECORD`.`RECORD_TYPE` = 4 AND `BODY_RECORD`.`USER_ID` = ".$request_user_id." AND YEAR(`BODY_RECORD`.`DATE`) = ". $row1['year'] ." AND MONTH(`BODY_RECORD`.`DATE`) = ". $row1['month'] ." ORDER BY `BODY_RECORD`.`DATE`, `BODY_RECORD`.`TIME` ASC";
          $result2 = select_result($sql);
          if (get_rowCount($result2)!=0) {
            while ($row2=get_row_mysqli_assoc($result2)) {
              $html = $html."<tr style='text-align: center; font-weight: 900; width: 300px;'>
              <td>".$row2['DATE']."</td>
              <td>".$row2['TIME']."</td>
              <td>".$row2['BP_H']."</td>
              <td>".$row2['BP_L']."</td>
              <td>".$row2['HEART_RATE']."</td>
              </tr>";
            }
          }

          $html = $html."</tbody>
          </table>";
        }
      }

      /// Food
      $sql = 'select YEAR(`FOOD_RECORD`.`FOOD_DATE`) as year, MONTH(`FOOD_RECORD`.`FOOD_DATE`) as month from `FOOD_RECORD` where `FOOD_RECORD`.`USER_ID` = '.$request_user_id.' and (FOOD_DATE BETWEEN "'.$from_year.'-'.$from_month.'-01 00:00:00" AND "'.$to_year.'-'.$to_month.'-31 23:59:59") GROUP BY YEAR(`FOOD_RECORD`.`FOOD_DATE`), MONTH(`FOOD_RECORD`.`FOOD_DATE`) ASC';
      //var_dump($sql);
      $result = select_result($sql);
      if (get_rowCount($result)!=0) {
        while ($row1=get_row_mysqli_assoc($result)) {
          $html = $html."<page-break-after></page-break-after>
          <p style='font-size:20px; font-weight: 900;'>飲食記錄 ".$row1['year']." / ".$row1['month']."</p><br />

          <table style='width: 1000px;'>
          <thead style='border-bottom:1pt solid black;'>
          <tr style='text-align: center; font-weight: 900; width: 300px;'>
          <th>日期</th>
          <th>時間</th>
          <th>類別</th>
          <th>地點</th>
          <th>食物</th>
          <th>數量</th>
          <th>碳水化合物</th>
          <th>脂肪</th>
          <th>蛋白質</th>
          <th>糖</th>
          </tr>
          </thead>
          <tbody>";

          $sql = "SELECT
          FOOD_RECORD.FOOD_RECORD_ID,
          FOOD_RECORD.USER_ID,
          FOOD_RECORD.FOOD_DATE,
          FOOD_RECORD.FOOD_TIME,
          FOOD_RECORD.FOOD_SESSION,
          FOOD_RECORD.FOOD_PLACE,
          FOOD_RECORD.FOOD_ID,
          FOOD_RECORD.FOOD_QUANTITY,
          FOOD_RECORD.FOOD_CALORIE,
          FOOD_RECORD.FOOD_CARBOHYDRATE,
          FOOD_RECORD.FOOD_PROTEIN,
          FOOD_RECORD.FOOD_FAT,
          FOOD_RECORD.FOOD_NAME,
          FOOD_RECORD.FOOD_UNIT,
          FOOD_RECORD.PHOTO_PATH,
          FOOD.FOOD_NAME as FOOD_NAME2,
          FOOD.FOOD_UNIT as FOOD_UNIT2,
          FOOD.FOOD_SUGAR as FOOD_SUGAR

          FROM FOOD_RECORD

          LEFT JOIN FOOD ON FOOD.FOOD_ID = FOOD_RECORD.FOOD_ID
          WHERE USER_ID = ".$request_user_id." AND YEAR(`FOOD_RECORD`.`FOOD_DATE`)  = ". $row1['year'] ." AND MONTH(`FOOD_RECORD`.`FOOD_DATE`) = ". $row1['month'] ." ORDER BY `FOOD_RECORD`.`FOOD_DATE`, `FOOD_RECORD`.`FOOD_TIME` ASC";
          $result2 = select_result($sql);
          if (get_rowCount($result2)!=0) {
            while ($row2=get_row_mysqli_assoc($result2)) {
              if ($row2['FOOD_ID'] != 0 && $row2['FOOD_ID'] != "") {
                $html = $html."<tr style='text-align: center; font-weight: 900; width: 300px;'>
                <td>".$row2['FOOD_DATE']."</td>
                <td>".$row2['FOOD_TIME']."</td>
                <td>".foodSessionToLabel($row2['FOOD_SESSION'])."</td>
                <td>".foodPlaceToLabel($row2['FOOD_PLACE'])."</td>
                <td>".$row2['FOOD_NAME2']."</td>
                <td>".$row2['FOOD_QUANTITY'].$row2['FOOD_UNIT2']."</td>
                <td>".$row2['FOOD_CARBOHYDRATE']."</td>
                <td>".$row2['FOOD_FAT']."</td>
                <td>".$row2['FOOD_PROTEIN']."</td>
                <td>".strval(doubleval($row2['FOOD_SUGAR'])*doubleval($row2['FOOD_QUANTITY']))."</td>
                </tr>";
              }else if ($row2['FOOD_QUANTITY'] == -1)
              {
                $html = $html."<tr style='text-align: center; font-weight: 900; width: 300px;'>
                <td>".$row2['FOOD_DATE']."</td>
                <td>".$row2['FOOD_TIME']."</td>
                <td>".foodSessionToLabel($row2['FOOD_SESSION'])."</td>
                <td>".foodPlaceToLabel($row2['FOOD_PLACE'])."</td>
                <td><img src='../../navFood/userFoodImage/".$row2['PHOTO_PATH'].".jpg' style='max-width:100px; max-height:100px; filter: grayscale(100%);' /></td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                </tr>";
              }else if (trim($row2['FOOD_NAME']) != "" && $row2['FOOD_NAME'] != "其他") {
                $html = $html."<tr style='text-align: center; font-weight: 900; width: 300px;'>
                <td>".$row2['FOOD_DATE']."</td>
                <td>".$row2['FOOD_TIME']."</td>
                <td>".foodSessionToLabel($row2['FOOD_SESSION'])."</td>
                <td>".foodPlaceToLabel($row2['FOOD_PLACE'])."</td>
                <td>".$row2['FOOD_NAME']."</td>
                <td>".$row2['FOOD_QUANTITY']."</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                </tr>";
              }else{
                $html = $html."<tr style='text-align: center; font-weight: 900; width: 300px;'>
                <td>".$row2['FOOD_DATE']."</td>
                <td>".$row2['FOOD_TIME']."</td>
                <td>".foodSessionToLabel($row2['FOOD_SESSION'])."</td>
                <td>".foodPlaceToLabel($row2['FOOD_PLACE'])."</td>
                <td><img src='../../navFood/userFoodImage/".$row2['PHOTO_PATH'].".jpg' style='max-width:100px; max-height:100px; filter: grayscale(100%);' /></td>
                <td>".$row2['FOOD_QUANTITY']."</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                </tr>";
              }

            }
          }

          $html = $html."</tbody>
          </table>";
        }
      }

      //EXERCISE
      $sql = 'select YEAR(`EXERCISE_TYPE_RECORD`.`EXERCISE_DATE`) as year, MONTH(`EXERCISE_TYPE_RECORD`.`EXERCISE_DATE`) as month from `EXERCISE_TYPE_RECORD` where `EXERCISE_TYPE_RECORD`.`USER_ID` = '.$request_user_id.' and  (EXERCISE_DATE BETWEEN "'.$from_year.'-'.$from_month.'-01 00:00:00" AND "'.$to_year.'-'.$to_month.'-31 00:00:00")  GROUP BY YEAR(`EXERCISE_TYPE_RECORD`.`EXERCISE_DATE`), MONTH(`EXERCISE_TYPE_RECORD`.`EXERCISE_DATE`) ASC';
      $result = select_result($sql);
      if (get_rowCount($result)!=0) {
        while ($row1=get_row_mysqli_assoc($result)) {
          $html = $html."<page-break-after></page-break-after>
          <p style='font-size:20px; font-weight: 900;'>運動記錄 ".$row1['year']." / ".$row1['month']."</p><br />

          <table style='width: 1000px;'>
          <thead style='border-bottom:1pt solid black;'>
          <tr style='text-align: center; font-weight: 900; width: 300px;'>
          <th>日期</th>
          <th>時間</th>
          <th>運動強度</th>
          <th>總運動時間</th>
          </tr>
          </thead>
          <tbody>";

          $sql = "SELECT
          `EXERCISE_TYPE_RECORD`.`USER_ID`,
      `EXERCISE_TYPE_RECORD`.`EXERCISE_TIME`,
      `EXERCISE_TYPE_RECORD`.`EXERCISE_RECORD_ID`,
      `EXERCISE_TYPE_RECORD`.`EXERCISE_PERIOD`,
      `EXERCISE_TYPE_RECORD`.`EXERCISE_DATE`,
          `EXERCISE_TYPE`.`EXERCISE_TYPE_NAME` as EXERCISE_TYPE_NAME

          FROM `EXERCISE_TYPE_RECORD`

          LEFT JOIN `EXERCISE_TYPE` ON `EXERCISE_TYPE`.`EXERCISE_TYPE_ID` = `EXERCISE_TYPE_RECORD`.`EXERCISE_TYPE_ID`
          WHERE USER_ID = ".$request_user_id." AND YEAR(`EXERCISE_TYPE_RECORD`.`EXERCISE_DATE`) = ". $row1['year'] ." AND MONTH(`EXERCISE_TYPE_RECORD`.`EXERCISE_DATE`) = ". $row1['month'] ." ORDER BY `EXERCISE_TYPE_RECORD`.`EXERCISE_DATE`, `EXERCISE_TYPE_RECORD`.`EXERCISE_TIME` ASC";

          $result2 = select_result($sql);
          if (get_rowCount($result2)!=0) {
            while ($row2=get_row_mysqli_assoc($result2)) {
              $html = $html."<tr style='text-align: center; font-weight: 900; width: 300px;'>
              <td>".$row2['EXERCISE_DATE']."</td>
              <td>".$row2['EXERCISE_TIME']."</td>
              <td>".$row2['EXERCISE_TYPE_NAME']."</td>
              <td>".$row2['EXERCISE_PERIOD']."</td>
              </tr>";

            }
          }

          $html = $html."</tbody>
          </table>";
        }
      }




      $html = $html."</body>
      </html>";

      define("DOMPDF_UNICODE_ENABLED", true);
      unset($_SESSION['report_userid']);

      $dompdf->loadHtml($html,'UTF-8');
      // (Optional) Setup the paper size and orientation
      $dompdf->setPaper('A4', 'landscape');
      // Render the HTML as PDF
      $dompdf->render();

      //Update access Date
      $sql = "INSERT INTO `REPORT_ACCESS_RECORDS` (LAST_ACCESS_DATETIME, USER_ID) VALUES(NOW(), ".$request_user_id.") ON DUPLICATE KEY UPDATE `REPORT_ACCESS_RECORDS`.`LAST_ACCESS_DATETIME` = NOW(), `REPORT_ACCESS_RECORDS`.`USER_ID` = ".$request_user_id;
      $result2 = select_result($sql);

      // Output the generated PDF to Browser
      $dompdf->stream("dompdf_out.pdf", array("Attachment" => false, "compress" => true));

    }
}
