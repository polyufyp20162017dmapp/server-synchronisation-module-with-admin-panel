// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// import Vue from 'vue'
var Vue = require('vue')
import App from './App'
import VeeValidate, { Validator } from 'vee-validate'
import vueResource from 'vue-resource'
import VueMask from 'v-mask'
var VueMaterial = require('vue-material')

Validator.extend('verify_password', {
  getMessage: field => 'The password must contain at least: 1 uppercase letter, 1 lowercase letter, 1 number, and one special character (E.g. , . _ & ? etc)',
  validate: value => {
    var strongRegex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#%&])(?=.{8,})')
    return strongRegex.test(value)
  }
})

/* eslint-disable no-new */
Vue.use(VueMaterial)
Vue.use(VeeValidate)
Vue.use(vueResource)
Vue.use(VueMask)

Vue.http.options.emulateJSON = true

new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})
