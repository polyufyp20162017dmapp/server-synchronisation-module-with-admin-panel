<?php
  require_once(dirname(__FILE__, 2).'/api/controller/auth.php');

  checkFolderPermission();

  if (!isFistTimeLogin()) {
    if (isset($_GET['redirect'])) {
      $path = "https://".$_SERVER['HTTP_HOST']."/dma/admin-panel/".urldecode($_GET['redirect']);
      header("Location: $path");
      exit();
    }
    header('HTTP/1.0 404 Not Found');
    exit();
  }

  if (isset($_GET['redirect'])) {
    $path = urldecode($_GET['redirect']);
    print "<script>var login_redirect = '$path'; </script>";
    print file_get_contents("index.page");
  }else{
    header('HTTP/1.0 404 Not Found');
    exit();
  }

  function checkFolderPermission(){
    $error = false;
    if (!is_writable(dirname(__FILE__, 2).'/api')) {
      $error = true;
      print "folder admin-panel/api required 755 permission <br />";
    }

    if (!is_writable(dirname(__FILE__, 3).'/navFood/userFoodImage')) {
      $error = true;
      print "folder navFood/userFoodImage required 755 permission <br />";
    }

    if ($error) {
      //header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
      die();
    }
  }

  function isFistTimeLogin(){
    if (file_exists(dirname(__FILE__, 2).'/api/FistTimeLogin.lock')) {
      return false;
    }else{
      return true;
    }
  }

?>
