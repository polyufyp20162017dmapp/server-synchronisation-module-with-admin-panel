<?php
require_once(dirname(__FILE__, 2).'/mysql.inc.php');
class syncDataVersionController
{
  public function __construct(&$requested_user_id, &$request_method, &$data)
  {
    switch ($request_method) {
      case 'getLatestVersion':
        $this->getLatestVersion();
        break;

        case 'getLatestExerciseType':
          $this->getLatestExerciseType();
          break;

        case 'getLatestFood':
          $this->getLatestFood();
          break;

          case 'getLatestFoodCategories':
            $this->getLatestFoodCategories();
            break;

          case 'getLatestFoodSubCategories':
            $this->getLatestFoodSubCategories();
            break;
      default:
      header('HTTP/1.0 404 Not Found');
      exit();
        break;
    }
  }

  public function getLatestVersion()
  {
    $sql = "SELECT * FROM SYNC_DATA_VERSION WHERE 1;";
    $result = select_result($sql);
    $output = array();
    if (get_rowCount($result)!=0) {
        while ($row1=get_row_mysqli_assoc($result)) {
            $output[$row1['TABLE']] = $row1['VERSION'];
        }
    }

    unset($sql);
    unset($result);
    print json_encode($output);
  }

  public function getLatestExerciseType()
  {
    $sql = "SELECT * FROM EXERCISE_TYPE WHERE 1;";
    $result = select_result($sql);
    $output = array();
    if (get_rowCount($result)!=0) {
        while ($row1=get_row_mysqli_assoc($result)) {
          $output[] = array('EXERCISE_TYPE_ID' => $row1['EXERCISE_TYPE_ID'], 'EXERCISE_TYPE_NAME' => $row1['EXERCISE_TYPE_NAME']);
        }
      }

      unset($sql);
      unset($result);
      print json_encode($output);

  }

  public function getLatestFood()
  {
    $sql = "SELECT * FROM FOOD WHERE 1;";
    $result = select_result($sql);
    $output = array();
    if (get_rowCount($result)!=0) {
        while ($row1=get_row_mysqli_assoc($result)) {
          $output[] = array('FOOD_ID' => $row1['FOOD_ID'], 'FOOD_NAME' => $row1['FOOD_NAME'],
          'FOOD_UNIT' => $row1['FOOD_UNIT'], 'FOOD_GRAM' => $row1['FOOD_GRAM'], 'FOOD_CALORIE' => $row1['FOOD_CALORIE'],
        'FOOD_CARBOHYDRATE' => $row1['FOOD_CARBOHYDRATE'], 'FOOD_PROTEIN' => $row1['FOOD_PROTEIN'], 'FOOD_FAT' => $row1['FOOD_FAT'],
      'FOOD_SUGAR' => $row1['FOOD_SUGAR'], 'FOOD_FIBER' => $row1['FOOD_FIBER'], 'FOOD_CHOLESTEROL' => $row1['FOOD_CHOLESTEROL'],
    'FOOD_SODIUM' => $row1['FOOD_SODIUM'], 'FOOD_SUB_CATE_ID' => $row1['FOOD_SUB_CATE_ID'], 'PHOTO_PATH' => $row1['PHOTO_PATH']);
        }
      }

      unset($sql);
      unset($result);
      print json_encode($output);
  }

  public function getLatestFoodCategories()
  {
    $sql = "SELECT * FROM FOOD_CATEGORIES WHERE 1;";
    $result = select_result($sql);
    $output = array();
    if (get_rowCount($result)!=0) {
        while ($row1=get_row_mysqli_assoc($result)) {
          $output[] = array('FOOD_CATE_ID' => $row1['FOOD_CATE_ID'],
        'FOOD_CATE_NAME' => $row1['FOOD_CATE_NAME']);
        }
      }

      unset($sql);
      unset($result);
      print json_encode($output);
  }

  public function getLatestFoodSubCategories()
  {
    $sql = "SELECT * FROM FOOD_SUB_CATEGORIES WHERE 1;";
    $result = select_result($sql);
    $output = array();

    if (get_rowCount($result)!=0) {
        while ($row1=get_row_mysqli_assoc($result)) {
          $output[] = array('FOOD_SUB_CATE_ID' => $row1['FOOD_SUB_CATE_ID'],
        'FOOD_SUB_CATE_NAME' => $row1['FOOD_SUB_CATE_NAME'],
      'FOOD_CATE_ID' => $row1['FOOD_CATE_ID']);
        }
      }

      unset($sql);
      unset($result);
      print json_encode($output);
  }
}

?>
