<?php

	$json = file_get_contents('php://input');
	$obj = json_decode($json);
		
	if ($obj!=null ){
		
		require_once('mysql.inc.php');
		require_once('forms.inc.php');
		
		$userId = $obj->{'userId'};
		//$userId = '123456';
		
		// Title of CSV
		$cls[] =  array('Account','Date','Time','Glucose_timeslot','Glucose_meal','Glucose_level',									  'BP_timeslot','BP_sys','BP_dia','Pulse','Drug', 'Insulin_name','Insulin_unit',									  'Food_categories','Food_session','Food_venue','Food_name','Food_qty', 'Food_unit','Exercise_mild','Exercise_moderate',				  'Exercise_vigorous');
		
		$sql_detail ="SELECT USER_ID,DATE,TIME,PERIOD,TYPE_GLUCOSE,GLUCOSE,BP_H,BP_L,HEART_RATE, '' AS Drug, '' AS INJECTION_NAME, '' AS INJECTION_VALUE ,'' as FOOD_CATE_NAME ,'' as FOOD_SUB_CATE_NAME ,'' as FOOD_SESSION,'' as FOOD_PLACE,'' as FOOD_NAME,'' as FOOD_QUANTITY,'' as FOOD_UNIT,''AS MILD,'' AS MODERATE,'' AS VIGOROUS FROM BODY_RECORD WHERE USER_ID='$userId' AND RECORD_TYPE IN (1,4)UNION ALL
		SELECT USER_ID,MEDICATION_DATE,'','','','','','','', IF(HAS_TAKE_MEDICATION = 'Y', '1', '0'),'','','','','','','','','','','','' FROM MEDICATION_RECORD WHERE USER_ID='$userId'
		UNION ALL
		SELECT USER_ID, INJECTION_DATE,INJECTION_TIME,'','','','','','','',INJECTION_NAME,INJECTION_VALUE,'','','','','','','','','','' FROM INJECTION_RECORD WHERE USER_ID='$userId'
		UNION ALL
		SELECT a.USER_ID, a.FOOD_DATE,a.FOOD_TIME,'','','','','','','','','', d.FOOD_CATE_NAME, c.FOOD_SUB_CATE_NAME, a.FOOD_SESSION, a.FOOD_PLACE, b.FOOD_NAME, a.FOOD_QUANTITY, b.FOOD_UNIT ,'','',''FROM FOOD_RECORD a, FOOD b, FOOD_SUB_CATEGORIES c, FOOD_CATEGORIES d where a.USER_ID='$userId' and a.FOOD_ID = b.FOOD_ID and a.FOOD_ID IS NOT NULL AND b.FOOD_SUB_CATE_ID = c.FOOD_SUB_CATE_ID AND c.FOOD_CATE_ID = d.FOOD_CATE_ID
		UNION ALL
		SELECT USER_ID, FOOD_DATE, FOOD_TIME ,'','','','','','','','','', 'NotDBitem', 'NotDBitem', FOOD_SESSION, FOOD_PLACE, FOOD_NAME, FOOD_QUANTITY, FOOD_UNIT,'','','' FROM FOOD_RECORD WHERE USER_ID='$userId' AND FOOD_ID IS NULL
		UNION ALL
		select USER_ID, EXERCISE_DATE, EXERCISE_TIME,'','','','','','','','','', '', '', '', '', '', '', '', IF(EXERCISE_TYPE_ID = '1', EXERCISE_PERIOD, ''), IF(EXERCISE_TYPE_ID = '2', EXERCISE_PERIOD, '') , IF(EXERCISE_TYPE_ID = '3', EXERCISE_PERIOD, '')  FROM EXERCISE_TYPE_RECORD WHERE USER_ID='$userId'
		order by DATE, TIME";
		
		$result_detail = select_result($sql_detail);
		if (get_rowCount($result_detail)!=0){
			while($row_detail = get_row_mysqli_assoc($result_detail)){
				$cls[] = $row_detail;
			}
		}
		
		header('Content-Type: application/csv; charset=UTF-8');
		header('Content-Disposition: attachment; filename="exportGeneralCSV.csv";');
		$f = fopen('php://output', "w");
		fprintf($f, chr(0xEF).chr(0xBB).chr(0xBF));
			foreach ($cls as $line) {
				fputcsv($f, $line);
			}
		fclose($f);
		closeConnectDB();
	}			
?>













